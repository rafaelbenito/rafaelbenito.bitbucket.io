function generarNumero(){
    let numero = Math.round(Math.random() * 60);
    if (numero >= 0 && numero < 10){
        numero = "0" + numero;
    }
    return numero;
}

function generarNumeros(){
    let circulos = document.querySelectorAll(".circulo");
    for (key in circulos){
        circulos[key].innerHTML=generarNumero();
    }
}

