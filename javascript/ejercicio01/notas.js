function notasSwitch(nota) {
    let resultado = '';
    switch (nota) {
        case (nota >= 0 && nota < 5):
            resultado = 'Reprobado';
            break;
        case (nota >= 5 && nota < 7.5):
            resultado = 'Aprobado';
            break;
        case (nota >= 7.5 && nota <= 10):
            resultado = 'Sobresaliente';
            break;
        default:
            resultado = 'Nota no válida';
    }
    return (resultado);
}

function notasElif(nota) {
    let resultado = '';
    if (nota < 5 && nota >= 0)
        resultado = 'Reprobado';
    else if (nota >= 5 && nota < 7.5)
        resultado = 'Aprobado';
    else if (nota >= 7.5 && nota <= 10)
        resultado = 'Sobresaliente';
    else
        resultado = 'Nota no válida';
    return (resultado);
}