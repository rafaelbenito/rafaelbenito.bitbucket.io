function notasSwitch(nota) {
    let resultado = '';
    switch (nota) {
        case (nota >= 0 && nota < 5):
            resultado = 'Reprobado';
            break;
        case (nota >= 5 && nota < 7.5):
            resultado = 'Aprobado';
            break;
        case (nota >= 7.5 && nota <= 10):
            resultado = 'Sobresaliente';
            break;
        default:
            resultado = 'Nota no válida';
    }
    return (resultado);
}

function notasElif(nota) {
    let resultado = '';
    if (nota < 5 && nota >= 0)
        resultado = 'Reprobado';
    else if (nota >= 5 && nota < 7.5)
        resultado = 'Aprobado';
    else if (nota >= 7.5 && nota <= 10)
        resultado = 'Sobresaliente';
    else
        resultado = 'Nota no válida';
    return (resultado);
}
function divisibilidad(numero, divisor) {
    divisible = (numero % divisor);
    if (divisible == 0){
        return true
    }
    else 
        return false;
}

function par(numero) {
    if (divisibilidad(numero, 2))
        return numero + " es par";
    else
        return numero + " no es par";

}

function divisibilidadTexto(numero, divisor) {
    if (divisibilidad(numero, divisor))
        return numero + " SÍ es divisible por " + divisor;
    else
        return numero + " NO es divisible por " + divisor;
}
function mayor(a, b) {
    if (a > b)
        return a + " es mayor que " + b;
    else if (a < b)
        return b + " es mayor que " + a;
    else
        return a + " es igual a " + b;
}

function datos(x){
    res1 = x + " " + par(x) + "\n";
    res2 = divisibilidadTexto(x, 3) + "\n";
    res3 = divisibilidadTexto(x, 5) + "\n";
    res4 = divisibilidadTexto(x, 7) + "\n";
    return res1 + res2 + res3 + res4;

}

function sumaValores(arr){
    let total = 0;
    for (key in arr)
        total+=arr[key];
    return total;
}

function esPrimo(x){
    numEsPrimo = true;
    for (var i = 3; i<x; i++)
        if (divisibilidad(x, i)){
            numEsPrimo = false;
            break;
        }
    return numEsPrimo;
}

function primo(x){
    if (numEsPrimo(x))
        return x + " es primo";
    else
        return x + " no es primo";
}

function terminoFibonacci(n){
    if (n < 3){
        return 1;
    }
    const raiz_5  = Math.sqrt(5);
    const factor1 = 1/raiz_5;
    let sumando   = ((1 + raiz_5)/2)**n;
    let minuendo  = ((1 - raiz_5)/2)**n
    return Math.round(factor1*(sumando - minuendo));
}

function fibonacci(n){
    let numerosDeFibonacci = '';
    for (var i = 1; i <= n; i++)
        if (i < n)
            numerosDeFibonacci+=terminoFibonacci(i) +', ';
        else numerosDeFibonacci+=terminoFibonacci(i)
    return numerosDeFibonacci;
}

function primoCifras(n){
    let primo = '';
    let min = 10 ** (n-1);
    let max = 0;
    for(var i = 0; i<n; i++){
        max += 9*10**i;
    }
    for(var j = min; j<=max; j++){
        if (esPrimo(j)){
            primo = j;
            break;
        }
    }
    if (primo == '')
        primo = 'No hay primos con ' + n + ' cifras';
    return primo;

}

function capitalizar(texto){
    texto1 = '';
    for(var i = 0; i<texto.length; i++){
        if (i == 0)
            texto1+=texto[i].toUpperCase();
        else
            texto1 += texto[i].toLowerCase();
    }
    return texto1;
}

function esVocal(letra){
    vocales = new Array('a', 'e', 'i', 'o', 'u');
    if (vocales.includes(letra.toLowerCase()))
        return 1;
    else
        return 0;
}

function palabra(x){
    let texto = '';
    let vocales = 0;
    let consonantes = 0;
    let total = x.length;
    for (var i = 0; i < total; i++){
        res = esVocal(x[i]);
        vocales += res;
        consonantes += !(res);
    }
    texto  = "La palabra " + x + " tiene " + total + " letras \n";
    texto  += par(total) + "\n";
    texto += "Tiene " + vocales + " vocales\n";
    texto += "Tiene " + consonantes + " consonantes";
    return texto;
}


function hoy(){
    var today = new Date();
    dias = ["domingo", "lunes","martes","miercoles","jueves","viernes","sabado"];
    return "Hoy es " + dias[today.getDay()]
}

function navidad(){
     
    var today = new Date();
    var navidad = new Date("2019-12-24");
    timeStamp = navidad - today;
    faltanDias = timeStamp/(1000 * 60 * 60 * 24);
    return "Faltan " + (parseInt(faltanDias) + 1) + " días";

}