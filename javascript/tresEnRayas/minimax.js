let casillasTotales = [
    [-1, 1,  0],  [0, 1,  0],  [1, 1,  0],
    [-1, 0,  0],  [0, 0,  0],  [1, 0,  0],
    [-1, -1, 0],  [0, -1, 0],  [1, -1, 0]
]

function ponerJugada(valor, casillas, num){
    let arboles = [];
    for (let i = 0; i < casillas.length; i++){
        arboles.push(casillas.slice());
    }
    if (num > 2){
        return arboles
    }
    else {
        return ponerJugada(-1 * valor, arboles, num + 1)
        
    }

}
