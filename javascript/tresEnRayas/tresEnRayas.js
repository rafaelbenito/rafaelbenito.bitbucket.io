function getPendiente(puntoA, puntoB){
    let x1 = puntoA.getAttribute("coordinateX");
    let y1 = puntoA.getAttribute("coordinateY");
    let x2 = puntoB.getAttribute("coordinateX");
    let y2 = puntoB.getAttribute("coordinateY");
    if (x1 - x2 == 0){
        return 3;
    }
    if (y2 - y1 == 0)
        return 0;
    m = (y2 - y1)/(x2 - x1);
    return m;
}

function hayGanador(caracter){
    let casillas = document.querySelectorAll("table tr td");
    let jugadas  = [];
    let aciertos  = 0;
    for (let i = 0; i < casillas.length; i++){
        if (casillas[i].innerHTML == caracter){
            jugadas[aciertos] = casillas[i];
            aciertos++;
        }
    }
    
    for (key in jugadas){
        var pendientes = [];
        for (key2 in jugadas){
            if (key == key2){
                continue;
            }
            pendienteEnStep = getPendiente(jugadas[key], jugadas[key2]);
            if (!pendientes.includes(pendienteEnStep)){
                pendientes.push(pendienteEnStep)
            }
            else {
                return 1;
            }
        }
        return 0;

    }
}

function jugadaPC(casilla, cursor){
    
    casilla.innerHTML=cursor;
    if (hayGanador(cursor)){
        document.getElementById("ganador").innerHTML="Ha ganado la PC";
    }
    return 1;
   
}
function hayCasillaEnBlanco(){
    let enBlanco = 0;
    let casillas = document.querySelectorAll("table tr td");
    for (let i = 0; i < casillas.length; i++){
        if (casillas[i].innerHTML != 'X' && casillas[i].innerHTML != 'O'){
            enBlanco++;
        }
    }
    return enBlanco;
}

function rellenar(element){
    if (hayGanador("X") || hayGanador("O")){
        alert("Juego terminado");
        return 1;
    }
    if (hayCasillaEnBlanco() == 0){
        alert("El juego ha terminado");
        return 1;
    }

    let casillas = document.querySelectorAll("table tr td");
    let texto = element.innerHTML;
    let cursores = document.querySelectorAll('input[type=radio]');
    if (texto == 'X' || texto == 'O'){
        return 1;
    }
    if (cursores[1].checked)
        cursor = "O";
    else
        cursor = "X";
    if (texto != 'X' && texto != 'O'){
        element.innerHTML = cursor;
        
    }
    if (hayGanador(cursor)){
        document.getElementById("ganador").innerHTML = "Has ganado";
        return 1;
    }
    let vacio = 0;
    while(vacio == 0){
        if (hayCasillaEnBlanco() == 0){
           
            return 1;
        }
        casillaSeleccionada = Math.floor ( Math.random() * 9 );
        if (casillas[casillaSeleccionada].innerHTML != 'X' && casillas[casillaSeleccionada].innerHTML != 'O' ){
            if (cursor == 'O')
                setTimeout(jugadaPC, 300, casillas[casillaSeleccionada], 'X');
            else
                setTimeout(jugadaPC, 300, casillas[casillaSeleccionada], 'O')                        
            vacio = 1;
        }

    }
}