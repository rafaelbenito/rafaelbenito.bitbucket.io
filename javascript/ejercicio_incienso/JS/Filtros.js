let Filtros = class {
    EMAIL_PATTERN = /(?!.*\.{2})^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i;       
    PRODUCTO_PATTERN = /[A-Z]{2}[0-9]{3}/;
    PRO_DESCRIPCION_PATTERN = /^[a-z 0-9]+$/i;
    mensajesDeErrores = [];

    filtroCorreo(correo){
        return (this.EMAIL_PATTERN.test(correo.toLowerCase()));
    }
    comunicarError(error){
        this.mensajesDeErrores.push(error);
    }
    rango(tipo, texto, min, max){
        if (texto.length < min || texto.length > max ){
            this.comunicarError("Los campos del tipo " + tipo + " debe tener mínimo " + min + " y máximo " + max )
            return false
        }
        else{
            return true;
        }
        
    }

    validacionMayusculas(texto){
        if (texto.toUpperCase() == texto)
            return true;
        else
            return false;
    }

    existe(texto){
        if (texto.length > 0){
            return 1;
        }
    }

    //Función que evalúa la validez del id de producto
    productoValido(id_producto){
        if (!this.PRODUCTO_PATTERN.test(id_producto) || (id_producto[0] != id_producto[0].toUpperCase() || id_producto[1] != id_producto[1].toUpperCase())){
            this.comunicarError("El id de producto debe tener el siguiente formato XX999");
            return false;
        }
        return true;
    }
    //Función para validad la descripción del producto
    descripcionProducto(texto){
        if (!this.rango("Descripción del producto", texto, 5, 100)){
            return false;
        }
        if (this.PRO_DESCRIPCION_PATTERN.test(texto)){
            return true;
        }
        else {
            this.comunicarError("La descripción del producto puede sólo ser alfanumérica y con espacios");
            return false;
        }
    }

    descripcionLarga(texto){
        if (!this.rango("Explicación del producto", texto, 5, 2000)){
            return false;
        }
        if (this.PRO_DESCRIPCION_PATTERN.test(texto)){
            return true;
        }
        else {
            this.comunicarError("Explicación del producto puede sólo ser alfanumérica y con espacios");
            return false;
        }
    }

}