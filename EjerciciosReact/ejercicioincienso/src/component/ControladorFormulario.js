import React from 'react';
import Formulario1 from './Formulario1';
class ControladorFormulario extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            formularioActivo:[false, false, false], 
            datos_producto:{
                idProducto:'XX999',
                descripcionProducto:'Matador de Sergios',
                descripcionProductoLarga:'Este producto se usa para matar Sergios'
            }
        };
        this.cambiarFormulario = this.cambiarFormulario.bind(this);
        this.mostrarContenidoHijo = this.mostrarContenidoHijo.bind(this);
    }
    mostrarContenidoHijo(e){
        console.log(e);
    }
    cambiarFormulario(e){
       let estadoCopia = this.state;
       for (let i = 0; i < estadoCopia.formularioActivo.length; i++){
        estadoCopia.formularioActivo[i] = false;
       }
       estadoCopia.formularioActivo[e.target.name] = true;
       this.setState(estadoCopia);
    }

    render(){
        return(
            <>
                <div className="row">
                    <div className="col-12 bg-dark sticky-top p-2 text-center">
                        <button onClick={this.cambiarFormulario} name="0" className="btn btn-dark">Formulario 1</button>
                        <button onClick={this.cambiarFormulario} name="1" className="btn btn-dark">Formulario 2</button>
                        <button onClick={this.cambiarFormulario} name="2" className="btn btn-dark">Formulario 3</button>
                    </div>
                </div>
                {this.state.formularioActivo[0] ? <Formulario1  estado={this.state} keyBoardHandler = {this.mostrarContenidoHijo} /> : null}
                {(!this.state.formularioActivo[0]) ? <div className="bg-info"><div className="col-12 text-center"> Presione un botón para empezar</div></div>:null}
            </>
        )
        
    }
}

export default ControladorFormulario;