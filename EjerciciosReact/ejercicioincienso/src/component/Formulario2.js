import React from 'react';
import "./css/Formulario1.css";
class Formulario1 extends React.Component{
    

    render(){
        return (
            <>
                <div action="" className="row align-items-center bg-warning ">
                    <div className="col-6 p-5 align-items-center text-center offset-3">
                        <input type="text" placeholder="ID de producto" name="id_producto" className="form-control mt-1" />
                        <input type="text" placeholder="Descripción del producto" name="pro_descripcion" className="form-control mt-1" />
                        <input type="text" placeholder="Descripción larga del producto" name="pro_desLarga" className="form-control mt-1" />
                        <input type="text" placeholder="Precio del producto" name="pro_precio" className="form-control mt-1" />
                        <input type="text" placeholder="Cantidad en Stock" name="pro_stock" className="form-control mt-1" />
                        <input type="date" placeholder="Fecha de reposición" name="pro_fecRepos" className="form-control mt-1" />
                        <input type="date" placeholder="Fecha de activación del producto" name="pro_fecActi" className="form-control mt-1" />
                        <input type="date" placeholder="Fecha de desactivación del producto" name="pro_fecDesacti" className="form-control mt-1" />
                        <input type="text" placeholder="Precio unitario de venta" name="pro_uniVenta" className="form-control mt-1" />
                        <input type="text" placeholder="Cantidad de unidades" name="pro_cantXUniVenta" className="form-control mt-1" />
                        <input type="text" placeholder="Último elemento" name="pro_uniUltNivel" className="form-control mt-1" />
                        <input type="text" placeholder="País de origen" name="id_pais" className="form-control mt-1" />
                        <input type="text" placeholder="Uso recomendado" name="pro_usoRecomendado" className="form-control mt-1" />
                        <input type="text" placeholder="ID de categoría" name="id_categoria" className="form-control mt-1" />
                        <input type="text" placeholder="Reservado en stock" name="pro_stkReservado" className="form-control mt-1" />
                        <input type="text" placeholder="Stock para nivel alto" name="pro_nStkAlto" className="form-control mt-1" />
                        <input type="text" placeholder="Stock para nivel bajo" name="pro_nStkBajo" className="form-control mt-1" />
                        <input type="text" placeholder="Estado" name="pro_stat" className="form-control mt-1" />
                        <input type="submit" value="Guardar" className="btn btn-small btn-guarda btn-primary" />
                    </div>
                </div>
            </>
        );
    }
}

export default Formulario1;