import React from 'react';

class FormularioClima extends React.Component {
    printData(){
        let ciudad = document.getElementsByName("ciudad")[0].value;
        console.log(ciudad);
    }
    render(){
        return(
        <div className="card">
            <div className="card-body">
                <div className="card-text">
                    
                        <div className="form-group">
                            <input className="form-control" placeholder="Introduzca la ciudad" name="ciudad" />
                            <input className="form-control" placeholder="Introduzca las iniciales del país" />
                            <button className="btn btn-warning form-control" onClick={this.printData}>Buscar</button>
                        </div>
                    
                </div>
            </div>
        </div>);
    }
}

export default FormularioClima;