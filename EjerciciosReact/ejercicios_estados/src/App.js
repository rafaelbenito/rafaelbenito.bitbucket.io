import React from "react";
import Thumbs from './components/Thumbs';
import Tricolor from './components/Tricolor';
import Fotos from './components/Fotos';
//import ListaCiudadJson from './components/ListaCiudadJson';
import Previsio from './Previsio';
export default () => (
  <>
    <Previsio />
    
    <Thumbs />
    <Tricolor />
    <Fotos />
  </>
);
