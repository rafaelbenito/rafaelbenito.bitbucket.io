import React from 'react';
import thumbsUp from './resources/thumbsUp.svg';
import thumbsDown from './resources/thumbsDown.svg';
import './css/thumbs.css';
class Thumbs extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            up:true,
            
        };
        this.changeState = this.changeState.bind(this);
    }
    changeState(){
        this.setState({up:!this.state.up});
    }
    render(){
        return(
            <> 
                <div className="container">
                    <h3>Thumbs</h3> <br />
                    <img alt="thumbs" className="thumbs" src={this.state.up ? thumbsUp : thumbsDown} onClick={this.changeState}></img>
                </div>
            </>
        );
    }

}

export default Thumbs;