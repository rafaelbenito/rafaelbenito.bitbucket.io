import React from 'react';
import thumbsUp from './resources/thumbsUp.svg';
import thumbsDown from './resources/thumbsDown.svg';
import thumbtack from './resources/thumbtack.svg';
import tumblr from './resources/tumblr.svg';
import './css/Fotos.css';
class Fotos extends React.Component{
    constructor(props){
        super(props);
        this.state = {imagen:'thumbsUp'};
    }

    changeFoto = e =>{
        console.log(e)
        this.setState({imagen:e.target.value});
    }

    render(){
        return(
            <>
            <br />
            <select onChange={this.changeFoto}>
                <option value="thumbsUp">thumbsUp</option>
                <option value="thumbsDown">thumbsDown</option>
                <option value="thumbtack">thumbtack</option>
                <option value="tumblr">tumblr</option>
            </select>
            <br />
            <br />
            {this.state.imagen === 'thumbsUp' ? <img src={thumbsUp}></img> : null}
            {this.state.imagen === 'thumbsDown' ? <img src={thumbsDown}></img> : null}
            {this.state.imagen === 'thumbtack' ? <img src={thumbtack}></img> : null}
            {this.state.imagen === 'tumblr' ? <img src={tumblr}></img> : null}
            </>
        )
    }
}

export default Fotos;