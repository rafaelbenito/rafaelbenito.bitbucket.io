import React from 'react';
import './css/Tricolor.css';
class Tricolor extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            0:'gris',
            1:'rojo',
            2:'verde',
            3:'azul',
            color:0
        }
        this.changeColor = this.changeColor.bind(this);
    }

    changeColor(){
        let stateViejo = this.state;
        stateViejo.color = stateViejo.color + 1;
        this.setState(stateViejo);
    }

    render(){
        return(
            <>
                <div onClick={this.changeColor} className={"bolita " + this.state[this.state.color % 4]}>
                </div>
            </>
        )
    }
}

export default Tricolor;