import React from 'react';
class ListaCiudadJson extends React.Component{
    constructor(props){
        super(props);
        this.state = {urlProvin:'http://kiosko.gestionproyectos.com/index.php?controller=API&action=listadoProvin', urlPobla:'http://kiosko.gestionproyectos.com/index.php?controller=API&action=listadoPoblaW&provin='
        }
        this.componentDidMount = this.componentDidMount.bind(this);
    }
    componentDidMount(){
        console.log(this.state)
        fetch(this.state.urlProvin)
        .then(resp => {return (resp.json())}
)
        .then(resp => {
            let provincias = resp.Cp_provincias;
            let estadoTemporal = this.state;
            estadoTemporal.provincias = provincias;
            estadoTemporal.provinciaSeleccionada = '1';
            let opcionesProvincias = provincias.map((item, i) => <option key={i} value={item.cppro_id}>{item.cppro_nombre}</option>)
            estadoTemporal.opcionesProvincias = opcionesProvincias;
            this.setState(estadoTemporal);
        })
        .finally( resp =>{
            console.log(this.state.urlPobla + '1')
            fetch(this.state.urlPobla + '1')
            .then(resp2 => {return resp2.json()})
            .then(resp2 =>{
                    let estadoTemporal = this.state;
                    let opcionesPoblaciones = resp2.Cp_poblacion.map((item) => {
                        return <option value={item.id} key={item.id}>{item.cppob_nombre}</option>
                    })
                    estadoTemporal.opcionesPoblaciones = opcionesPoblaciones;
                    this.setState(estadoTemporal);
                }
            )}
        )
    }
    getComunidades = event =>{
        let estadoTemporalInicio = this.state
        estadoTemporalInicio.provinciaSeleccionada = event.target.value;
        this.setState(estadoTemporalInicio);
        console.log(this.state.urlPobla  + this.state.provinciaSeleccionada)
        fetch(this.state.urlPobla + this.state.provinciaSeleccionada)
            .then(resp2 => {return resp2.json()})
            .then(
                resp2 =>{
                    let estadoTemporal = this.state;
                    let opcionesPoblaciones = resp2.Cp_poblacion.map((item) => {
                        return <option value={item.id} key={item.id}>{item.cppob_nombre}</option>
                    })
                    estadoTemporal.opcionesPoblaciones = opcionesPoblaciones;
                    this.setState(estadoTemporal);
                }
            )
    }
    render(){
        return (
            <>
                <select onChange={this.getComunidades}>
                    {this.state.opcionesProvincias}
                </select>
                <select>
                    {this.state.opcionesPoblaciones}
                </select>
            </>
        )
    }
}

export default ListaCiudadJson;