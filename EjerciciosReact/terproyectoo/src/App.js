import React from 'react';
import logo from './logo.svg';
import './App.css';
import Botones from "./components/Botones.js";

function App() {
  return (
    <div>
      <Botones />
    </div>
    
  );
}

export default App;
