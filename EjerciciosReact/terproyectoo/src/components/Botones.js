import React from "react";
import Coordenadas from "./Coordenadas";
import Formulario from "./Formulario";

class Botones extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            Coordenadas:false,
            Formulario:false
        }
       this.mostrarCoordenadas = this.mostrarCoordenadas.bind(this);
       this.mostrarFormulario = this.mostrarFormulario.bind(this);
    }
    mostrarCoordenadas(){
        
        this.setState({
            Coordenadas:!this.state.Coordenadas,
            Formulario : false
        })
    }
    mostrarFormulario(){
        this.setState({
            Formulario : !this.state.Formulario,
            Coordenadas : false
        })
    }
    render(){
        return(
            <div>
                <div>
                    <button onClick={this.mostrarCoordenadas}>Mostrar Coordenadas</button>
                    <button onClick={this.mostrarFormulario}>Mostrar Formulario</button>
                </div>
                <div>
                    {this.state.Coordenadas ? <Coordenadas /> : ''}
                    {this.state.Formulario ? <Formulario /> : ''}
                </div>
            </div>
            
        )
    }
}

export default Botones;