import React from "react";

class Formulario extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            nombre:null
        }
    }
    changeState(e){
        this.setState({nombre: e.target.value})
    }
    render(){
        return(
            <div>
                <input type="text" placeholder="Nombre" onChange={e => this.changeState(e)} />
                {this.state.nombre ? <h4>Hola {this.state.nombre}</h4>:''}
            </div>
        )
    }
}

export default Formulario;